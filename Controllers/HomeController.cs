﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assessment.Classes;
using ForecastIO;
using Assessment.Database;

namespace Assessment.Controllers
{
  public class HomeController : Controller
  {
    private Forecast forecast = new Forecast();
    public ActionResult Index()
    { 
      return View(forecast.GetForecastWeek());
    }

    [HttpPost]
    public ActionResult Post()
    {
      var week = forecast.GetForecastWeek();
      ForecastEntities context = new ForecastEntities();
      foreach (var day in week)
      {
       
        context.Set<datatable>().Add(new datatable { day = day.time.ToString(), windSpeed = day.windSpeed.ToString(), summary = day.summary, maxTemp = day.temperatureMax.ToString(), minTemp = day.temperatureMin.ToString() });
        context.SaveChanges();
      }
      return RedirectToAction("Index");
    }
  }
