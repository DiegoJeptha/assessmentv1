﻿using Assessment.Interface;
using ForecastIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment.Classes
{
  public class Forecast : IForecast
  {
    public List<DailyForecast> GetForecastWeek()
    {
      //Cape Town Forecast
      //GET
      var request = new ForecastIORequest("4b75b82842cd6e50baa53b4f3d2ca774", -33.9142688f, 18.0956147f, Unit.auto);

      var f = request.Get();
      var listOfDailyForecasts = f.daily.data;
      return listOfDailyForecasts;
    }
  }
}


