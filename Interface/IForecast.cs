﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForecastIO;

namespace Assessment.Interface
{
  public interface IForecast
  {
   List<DailyForecast> GetForecastWeek();
  }
}
